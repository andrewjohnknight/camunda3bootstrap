<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:camunda="http://camunda.org/schema/1.0/bpmn" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1dzjhoc" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="3.7.2">
  <bpmn:process id="GuitarPackageCostCalculator" name="Example Guitar Package Cost Calculator - DMN uses collect and sum " isExecutable="true">
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:outgoing>Flow_12dtn93</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:sequenceFlow id="Flow_12dtn93" sourceRef="StartEvent_1" targetRef="Activity_0fxy83i" />
    <bpmn:userTask id="Activity_0fxy83i" name="Get Guitar Package requirement">
      <bpmn:extensionElements>
        <camunda:formData>
          <camunda:formField id="yourName" label="Your name" type="string" />
          <camunda:formField id="guitarName" label="Name of Guitar" type="enum">
            <camunda:value id="Gibson" name="Gibson" />
            <camunda:value id="Fender" name="Fender (made in USA)" />
            <camunda:value id="Seagull" name="Seagull (Made in Canada)" />
          </camunda:formField>
          <camunda:formField id="carryType" label="Type of case required" type="enum">
            <camunda:value id="none" name="not required" />
            <camunda:value id="soft" name="soft case - backpack style" />
            <camunda:value id="hard" name="hard case" />
          </camunda:formField>
          <camunda:formField id="stringSet" label="Sets of extra sets of strings (0, 1, 2, or 3)" type="long" defaultValue="1" />
          <camunda:formField id="bestQuality" label="Prefer best quality at higher price" type="boolean" defaultValue="true" />
        </camunda:formData>
      </bpmn:extensionElements>
      <bpmn:incoming>Flow_12dtn93</bpmn:incoming>
      <bpmn:incoming>Flow_0lkd5ux</bpmn:incoming>
      <bpmn:outgoing>Flow_0u333sf</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:sequenceFlow id="Flow_0u333sf" sourceRef="Activity_0fxy83i" targetRef="Activity_0gn2p6l" />
    <bpmn:businessRuleTask id="Activity_0gn2p6l" name="Cost Guitar Package" camunda:resultVariable="sumOfComponents" camunda:decisionRef="ExampleGuitarPrice" camunda:mapDecisionResult="singleEntry">
      <bpmn:incoming>Flow_0u333sf</bpmn:incoming>
      <bpmn:outgoing>Flow_138o7i7</bpmn:outgoing>
    </bpmn:businessRuleTask>
    <bpmn:sequenceFlow id="Flow_138o7i7" sourceRef="Activity_0gn2p6l" targetRef="Activity_0qvpzhp" />
    <bpmn:userTask id="Activity_0qvpzhp" name="Provide Guitar package offer">
      <bpmn:extensionElements>
        <camunda:formData>
          <camunda:formField id="yourName" label="Your Name" type="string" />
          <camunda:formField id="guitarName" label="Name of Guitar" type="string" />
          <camunda:formField id="carryType" label="Guitar case" type="string" />
          <camunda:formField id="stringSet" label="No of sets of strings 0 - 3" type="long" />
          <camunda:formField id="bestQuality" label="Best quality at higher price quoted where option exists" type="boolean" />
          <camunda:formField id="sumOfComponents" label="Total cost of all components" type="long" />
          <camunda:formField id="tryAgain" label="Would you like to try another combination ?" type="boolean" />
        </camunda:formData>
      </bpmn:extensionElements>
      <bpmn:incoming>Flow_138o7i7</bpmn:incoming>
      <bpmn:outgoing>Flow_14lmxrl</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:exclusiveGateway id="Gateway_0zdonfm">
      <bpmn:incoming>Flow_14lmxrl</bpmn:incoming>
      <bpmn:outgoing>Flow_1mcul43</bpmn:outgoing>
      <bpmn:outgoing>Flow_0lkd5ux</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:sequenceFlow id="Flow_14lmxrl" sourceRef="Activity_0qvpzhp" targetRef="Gateway_0zdonfm" />
    <bpmn:endEvent id="Event_1va9ipd">
      <bpmn:incoming>Flow_1mcul43</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="Flow_1mcul43" sourceRef="Gateway_0zdonfm" targetRef="Event_1va9ipd">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">${tryAgain == false}</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="Flow_0lkd5ux" name="Try another option" sourceRef="Gateway_0zdonfm" targetRef="Activity_0fxy83i">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression">${tryAgain == true}</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
  </bpmn:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="GuitarPackageCostCalculator">
      <bpmndi:BPMNEdge id="Flow_0lkd5ux_di" bpmnElement="Flow_0lkd5ux">
        <di:waypoint x="780" y="142" />
        <di:waypoint x="780" y="250" />
        <di:waypoint x="320" y="250" />
        <di:waypoint x="320" y="157" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="506" y="232" width="88" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_1mcul43_di" bpmnElement="Flow_1mcul43">
        <di:waypoint x="805" y="117" />
        <di:waypoint x="872" y="117" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_14lmxrl_di" bpmnElement="Flow_14lmxrl">
        <di:waypoint x="690" y="117" />
        <di:waypoint x="755" y="117" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_138o7i7_di" bpmnElement="Flow_138o7i7">
        <di:waypoint x="530" y="117" />
        <di:waypoint x="590" y="117" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_0u333sf_di" bpmnElement="Flow_0u333sf">
        <di:waypoint x="370" y="117" />
        <di:waypoint x="430" y="117" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Flow_12dtn93_di" bpmnElement="Flow_12dtn93">
        <di:waypoint x="215" y="117" />
        <di:waypoint x="270" y="117" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1">
        <dc:Bounds x="179" y="99" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1m2zxyb_di" bpmnElement="Activity_0fxy83i">
        <dc:Bounds x="270" y="77" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_0xsy6ky_di" bpmnElement="Activity_0gn2p6l">
        <dc:Bounds x="430" y="77" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Activity_1gzhysr_di" bpmnElement="Activity_0qvpzhp">
        <dc:Bounds x="590" y="77" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Gateway_0zdonfm_di" bpmnElement="Gateway_0zdonfm" isMarkerVisible="true">
        <dc:Bounds x="755" y="92" width="50" height="50" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Event_1va9ipd_di" bpmnElement="Event_1va9ipd">
        <dc:Bounds x="872" y="99" width="36" height="36" />
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn:definitions>
